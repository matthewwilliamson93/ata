interface GeocodeAddress {
    place: string;
    county: string;
    city: string;
    city_district: string;
    country: string;
    state: string;
    town: string;
    village: string;
}

interface NominatimResponse {
    address: GeocodeAddress;
    class: string;
    display_name: string;
    importance: number;
    lat: string;
    lon: string;
    osm_id: string;
    osm_type: string;
    place_id: string;
    type: string;
}

export type Site = {
    id: string;
    name: string;
    inbetween: string | null;
    country: string;
    lat: number;
    long: number;
    islocked: boolean;
    haveBeen: boolean;
    top: number;
    height: number;
};

/*
 * Converts a Nominatim site to a site.
 *
 * Args:
 *    site: The site to convert.
 *
 * Returns:
 *    The converted site.
 */
function nominatimSiteToSite(site: NominatimResponse): Site | null {
    if (site.address === undefined) {
        return null;
    }

    const address = site.address;
    if (
        (site.class !== "boundary" && site.class !== "place") ||
        site.osm_type !== "relation" ||
        site.osm_id === undefined ||
        (address.city === undefined &&
            address.village === undefined &&
            address.town === undefined &&
            address.place === undefined) ||
        address.country === undefined ||
        site.lat === undefined ||
        site.lon === undefined
    ) {
        return null;
    }

    return {
        id: site.osm_id,
        name: address.city || address.village || address.town || address.place,
        inbetween: address.state || address.county || null,
        country: address.country,
        lat: parseFloat(site.lat),
        long: parseFloat(site.lon),
        islocked: false,
        haveBeen: false,
        top: 0,
        height: 0,
    };
}

function eqSites(x: Site, y: Site): boolean {
    return x.name === y.name && x.inbetween === y.inbetween && x.country === y.country;
}

/*
 * Search Nominatim to find possible sites matching the query.
 *
 * https://nominatim.org/release-docs/latest/api/Search/
 *
 * Args:
 *     query: The search string to lookup.
 *
 * Returns:
 *     The promise of an array of possible sites to choose from.
 */
export async function search(query: string): Promise<Array<Site>> {
    if (query.length < 2) {
        return [];
    }

    const params = {
        q: query,
        limit: 25,
        addressdetails: 1,
        format: "json",
    };
    const paramsStr = Object.entries(params)
        .map(([k, v]) => `${encodeURIComponent(k)}=${encodeURIComponent(v)}`)
        .join("&");
    const response = await fetch(`https://nominatim.openstreetmap.org/search?${paramsStr}`, {
        method: "GET",
        headers: {
            "Content-Type": "application/json",
        },
        redirect: "follow",
    });
    const results = await response.json();

    return results.reduce((acc, osmSite) => {
        if (6 < acc.length) {
            return acc;
        }

        const site = nominatimSiteToSite(osmSite);
        if (site === null) {
            return acc;
        }

        if (acc.length === 0) {
            return [site];
        }

        const lastSite = acc[acc.length - 1];
        if (eqSites(site, lastSite)) {
            return acc;
        }

        return [...acc, site];
    }, [] as Array<Site>);
}
