import Grid from "@mui/material/Grid";

import { createStyles, makeStyles } from "@mui/styles";

import React from "react";

import TravelAppBar from "./components/TravelAppBar";
import TravelDrawer from "./components/TravelDrawer";
import TravelLegend from "./components/TravelLegend";
import TravelList from "./components/TravelList";
import TravelMap from "./components/TravelMap";
import TravelPlannerButton from "./components/TravelPlannerButton";

import { Route } from "./utils/routes";
import { useStickyState } from "./hooks/useStickyState";

const useStyles = makeStyles(() =>
    createStyles({
        root: {
            background: "white",
            flexGrow: 1,
            height: "100vh",
            position: "relative",
        },
        header: {
            "margin-bottom": "50px !important",
            font: '375 3.2em/1.2 "Lato", sans-serif',
            "text-shadow": "-2px 0 white, 0 2px white, 2px 0 white, 0 -2px white",
            color: "black",
            margin: 0,
            position: "relative",
            "text-align": "center",
        },
    }),
);

/*
 * A React component that is meant to house the main layout for the site.
 *
 * https://mui.com/components/grid/
 * https://mui.com/styles/api/
 */
export default function App(): JSX.Element {
    const classes = useStyles();

    const [bucketList, setBucketList] = useStickyState<Route>([], "tbl-bucket-list");

    const [currentTrip, setCurrentTrip] = useStickyState<string | null>(null, "tbl-current-trip");
    const [trips, setTrips] = useStickyState<{ [trip: string]: Route }[]>([], "tbl-trips");

    const setTrip = (route) => {
        if (currentTrip === null) {
            return;
        }

        setTrips({ ...trips, [currentTrip]: route });
    };

    const [isDrawerActive, setIsDrawerActive] = useStickyState<boolean>(
        false,
        "tbl-is-drawer-active",
    );

    return (
        <div className={classes.root}>
            <TravelMap
                isHome={currentTrip === null}
                route={currentTrip === null ? bucketList : trips[currentTrip] || []}
            />
            <TravelDrawer
                trips={Object.keys(trips)}
                active={isDrawerActive}
                setActive={setIsDrawerActive}
                setCurrentTrip={setCurrentTrip}
            />
            <Grid container>
                <TravelAppBar
                    currentTrip={currentTrip}
                    isMenuActive={isDrawerActive}
                    onMenuClick={setIsDrawerActive}
                    route={currentTrip === null ? bucketList : trips[currentTrip] || []}
                    setRoute={currentTrip === null ? setBucketList : setTrip}
                />
                <Grid
                    alignItems="flex-start"
                    container
                    justifyContent="flex-start"
                    gap={3}
                    marginTop="80px"
                >
                    <TravelList
                        isHome={currentTrip === null}
                        setTrip={setCurrentTrip}
                        route={currentTrip === null ? bucketList : trips[currentTrip] || []}
                        setRoute={currentTrip === null ? setBucketList : setTrip}
                    />
                    <Grid item marginLeft="auto">
                        <TravelLegend />
                    </Grid>
                </Grid>
            </Grid>
        </div>
    );
}
