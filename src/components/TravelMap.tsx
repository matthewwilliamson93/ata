import { Feature } from "geojson";
import maplibregl from "maplibre-gl";

import { createStyles, makeStyles } from "@mui/styles";

import React, { useRef, useEffect, useState } from "react";

import { renderToStaticMarkup } from "react-dom/server";

import { useStickyState } from "../hooks/useStickyState";
import { Route } from "../utils/routes";
import { Site } from "../utils/sites";
import TravelPin from "./TravelPin";

export type MapSite = {
    name: string;
    inbetween: string | null;
    country: string;
    lat: number;
    long: number;
    haveBeen: boolean;
};

function RecenterLogo(): JSX.Element {
    return (
        <svg
            id="recenter"
            data-name="recenter"
            role="img"
            aria-labelledby="recenter-title"
            xmlns="http://www.w3.org/2000/svg"
            width="29px"
            height="29px"
            viewBox="0 0 512 512"
        >
            <title id="recenter-title">Recenter the map</title>
            <polyline points="304 416 304 304 416 304" />
            <line x1="314.2" y1="314.23" x2="432" y2="432" />
            <polyline points="208 96 208 208 96 208" />
            <line x1="197.8" y1="197.77" x2="80" y2="80" />
            <polyline points="416 208 304 208 304 96" />
            <line x1="314.23" y1="197.8" x2="432" y2="80" />
            <polyline points="96 304 208 304 208 416" />
            <line x1="197.77" y1="314.2" x2="80" y2="432" />
        </svg>
    );
}

const useStyles = makeStyles(() =>
    createStyles({
        "map-container": {
            position: "absolute",
            top: 0,
            bottom: 0,
            left: 0,
            right: 0,
        },
        "map-recenter": {
            "background-repeat": "no-repeat",
            "background-position": "center",
            "pointer-events": "auto",
            "background-image": `url("${svgToData(RecenterLogo())}")`,
        },
    }),
);

/*
 *  A function to convert an SVG to an the data to encode it as an image.
 *
 * Args:
 *     svg: The SVG to convert.
 *
 * Returns:
 *     The data encoding the SVG.
 */
function svgToData(svg: JSX.Element): string {
    let svgString = renderToStaticMarkup(svg);
    svgString = svgString
        .replace(/"/g, "'")
        .replace(/%/g, "%25")
        .replace(/#/g, "%23")
        .replace(/{/g, "%7B")
        .replace(/}/g, "%7D")
        .replace(/</g, "%3C")
        .replace(/>/g, "%3E")
        .replace(/\s+/g, " ")
        .replace(/&/g, "%26")
        .replace("|", "%7C")
        .replace("[", "%5B")
        .replace("]", "%5D")
        .replace("^", "%5E")
        .replace("`", "%60")
        .replace(";", "%3B")
        .replace("?", "%3F")
        .replace(":", "%3A")
        .replace("@", "%40")
        .replace("=", "%3D");

    return `data:image/svg+xml;charset=utf-8,${svgString}`;
}

/**
 * Makes appropriate Map bounds based on the listed route.
 *
 * Args:
 *     route: The route of sites in the map.
 *
 * Returns:
 *     The Mapbox bounds.
 */
function makeMapBounds(route: MapSite[]): maplibregl.LngLatBounds {
    const bounder = (site: MapSite) => new maplibregl.LngLat(site.long, site.lat);
    const starterBound = bounder(route[0]);
    return route.reduce(
        (bounds, site) => bounds.extend(bounder(site)),
        new maplibregl.LngLatBounds(starterBound, starterBound),
    );
}

/*
 * A class for an IControl for the maplibre map to display a button that will
 * recenter the map.
 *
 * https://maplibre.org/maplibre-gl-js-docs/api/markers/#icontrol
 */
class RecenterButtonControl {
    className: string;
    route: MapSite[];
    container?: HTMLElement;
    map?: maplibregl.Map;

    /*
     * The constructor for the class.
     *
     * Args:
     *     className: The name of the css class.
     *     route: The route being taken.
     */
    constructor(className: string, route: MapSite[]) {
        this.className = className;
        this.route = route;
        this.container = undefined;
        this.map = undefined;
    }

    /*
     * What to add to the map.
     *
     * Args:
     *     map: The map to add the button to.
     *
     * Returns:
     *     The HTML element to render.
     */
    onAdd(map): HTMLElement {
        this.map = map;

        const btn = document.createElement("button");
        btn.className = this.className;
        btn.type = "button";
        btn.title = "Recenter Map";
        btn.onclick = () => {
            map.fitBounds(makeMapBounds(this.route), {
                padding: 100,
                maxZoom: 5,
            });
        };

        this.container = document.createElement("div");
        this.container.className = "maplibregl-ctrl-group maplibregl-ctrl map-renter";
        this.container.appendChild(btn);

        return this.container;
    }

    /*
     * Handles the cleanup of the button.
     */
    onRemove(): void {
        if (this.container === undefined || this.container === null) {
            return;
        }

        this.container.parentNode?.removeChild(this.container);
        this.map = undefined;
    }
}

/*
 * A React component that is meant to show all th sites on a Map with
 * the route.
 *
 * https://maplibre.org/maplibre-gl-js-docs/api/
 * https://mui.com/styles/api/
 */
export default function TravelMap({
    isHome,
    route,
}: {
    isHome: boolean;
    route: Route;
}): JSX.Element {
    const classes = useStyles();

    const mapContainerRef = useRef(null);
    const [center, setCenter] = useStickyState<maplibregl.LngLatLike>([5, 34], "tbl-saved-center");
    const [zoom, setZoom] = useStickyState<number>(1.5, "tbl-saved-zoom");
    const [internalRoute, setInternalRoute] = useState<MapSite[]>([...route]);

    useEffect(() => {
        const tempRoute = route.map((site: Site) => ({
            name: site.name,
            inbetween: site.inbetween,
            country: site.country,
            lat: site.lat,
            long: site.long,
            haveBeen: site.haveBeen,
        }));
        if (isHome) {
            tempRoute.sort((a, b) => (a.name > b.name ? 1 : -1));
        }
        if (JSON.stringify(tempRoute) === JSON.stringify(internalRoute)) {
            return;
        }

        setInternalRoute(tempRoute);
    }, [route]);

    // Initialize map when component mounts
    useEffect(() => {
        const map = new maplibregl.Map({
            //@ts-ignore TS2322
            container: mapContainerRef.current,
            style: "https://demotiles.maplibre.org/style.json",
            center,
            zoom,
        });

        map.addControl(new maplibregl.NavigationControl({}), "bottom-right");
        map.addControl(
            new RecenterButtonControl(classes["map-recenter"], internalRoute),
            "bottom-right",
        );
        map.on("load", () => {
            if (internalRoute.length === 0) {
                return 0;
            }

            if (!isHome) {
                // Add the route as a line
                map.addSource("travel-lines-data", {
                    type: "geojson",
                    data: {
                        type: "Feature",
                        geometry: {
                            type: "LineString",
                            coordinates: internalRoute.map((site: MapSite) => [
                                site.long,
                                site.lat,
                            ]),
                        },
                        properties: {},
                    },
                });
                map.addLayer({
                    id: "travel-lines-layer",
                    source: "travel-lines-data",
                    type: "line",
                    layout: {
                        "line-join": "round",
                        "line-cap": "round",
                    },
                    paint: {
                        "line-color": "#33C9EB",
                        "line-width": 6,
                    },
                });
            }

            // Add sites as markers
            internalRoute.forEach((site) => {
                // create a DOM element for the marker
                const marker = document.createElement("div");
                marker.className = "marker";
                const color = site.haveBeen ? "#4899f1" : "#eb4d70";
                marker.style.backgroundImage = `url("${svgToData(TravelPin({ color }))}")`;
                marker.style.width = "20px";
                marker.style.height = "48px";
                marker.style.cursor = "pointer";

                marker.addEventListener("click", () => {
                    map.flyTo({
                        center: [site.long, site.lat],
                    });
                });

                new maplibregl.Marker(marker).setLngLat([site.long, site.lat]).addTo(map);
            });

            // Setup bounds
            map.fitBounds(makeMapBounds(internalRoute), {
                padding: 100,
                maxZoom: 5,
            });
        });

        map.on("moveend", () => {
            setCenter(map.getCenter());
            setZoom(map.getZoom());
        });

        // Clean up on unmount
        return () => map.remove();
    }, [internalRoute]);

    return <div className={classes["map-container"]} ref={mapContainerRef} />;
}
