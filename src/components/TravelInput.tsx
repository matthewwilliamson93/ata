import { alpha, styled } from "@mui/material/styles";
import Autocomplete from "@mui/material/Autocomplete";
import CircularProgress from "@mui/material/CircularProgress";
import TextField from "@mui/material/TextField";
import SearchIcon from "@mui/icons-material/Search";

import React, { Dispatch, SetStateAction, useRef, useState } from "react";

import { useAsync } from "../hooks/useAsync";
import { useDebounce } from "../hooks/useDebounce";
import { useKeypress } from "../hooks/useKeypress";
import { Route } from "../utils/routes";
import { Site, search } from "../utils/sites";

const Search = styled("div")(({ theme }) => ({
    position: "relative",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: alpha(theme.palette.common.white, 0.15),
    "&:hover": {
        backgroundColor: alpha(theme.palette.common.white, 0.25),
    },
    marginLeft: "20px",
    width: "400px",
}));

const SearchIconWrapper = styled("div")(({ theme }) => ({
    padding: theme.spacing(0, 2),
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
}));

const StyledInputBase = styled("div")(({ theme }) => ({
    color: "inherit",
    "& .MuiFormLabel-root": {
        marginLeft: "30px",
    },
}));

/*
 * A React component that is meant to handle the input for search of sites.
 *
 *
 * https://mui.com/components/
 * https://mui.com/styles/api/
 */
export default function TravelInput({
    isHome,
    route,
    setRoute,
}: {
    isHome: boolean;
    route: Route;
    setRoute: Dispatch<SetStateAction<Route>>;
}): JSX.Element {
    const placeholder = `Press "/" to add a destination to your ${isHome ? "bucket list" : "trip"}`;

    const ref = useRef<HTMLInputElement>(null);
    const [open, setOpen] = useState(false);
    const [input, setInput] = useState("");
    const [query, setQuery] = useState("");
    const debouncedQuery = useDebounce<string>(query, 300);

    const { loading, value } = useAsync<Array<Site>>(
        async () => await search(debouncedQuery),
        [debouncedQuery],
    );

    useKeypress("Escape", (e: KeyboardEvent) => {
        if (e === undefined) {
            return;
        }

        if (ref.current === null) {
            e.preventDefault();
            return;
        }

        ref.current?.blur();
    });

    useKeypress("/", (e: KeyboardEvent) => {
        if (e === undefined) {
            return;
        }

        if (document.activeElement === ref.current || ref.current === null) {
            e.preventDefault();
            return;
        }

        ref.current?.focus();
    });

    return (
        <Search style={{ width: "500px" }}>
            <StyledInputBase>
                <Autocomplete
                    id="travel-input"
                    fullWidth={true}
                    getOptionLabel={(option) =>
                        option.inbetween ? `${option.name} - ${option.inbetween}` : option.name
                    }
                    groupBy={(option) => option.country}
                    loading={loading}
                    inputValue={input}
                    isOptionEqualToValue={(x, y) => x.id === y.id}
                    onChange={async (event, site) => {
                        if (site !== null) {
                            setRoute([...route, site]);
                        }

                        setQuery("");
                        setInput("");
                    }}
                    onInputChange={(event, newInput, reason) => {
                        if (newInput.length > 1) {
                            setQuery(newInput);
                        }

                        setInput(reason === "reset" ? "" : newInput);
                    }}
                    onOpen={() => {
                        setOpen(true);
                    }}
                    onClose={() => {
                        setOpen(false);
                    }}
                    open={open}
                    options={value || []}
                    renderInput={(params) => (
                        <TextField
                            {...params}
                            onFocus={() => {
                                setInput("");
                            }}
                            onBlur={() => {
                                setInput("");
                            }}
                            InputLabelProps={{
                                ...params.InputLabelProps,
                            }}
                            InputProps={{
                                ...params.InputProps,
                                endAdornment: (
                                    <React.Fragment>
                                        {loading ? (
                                            <CircularProgress color="inherit" size={20} />
                                        ) : (
                                            <SearchIcon />
                                        )}
                                        {params.InputProps.endAdornment}
                                    </React.Fragment>
                                ),
                            }}
                            label={placeholder}
                            inputRef={ref}
                        />
                    )}
                    selectOnFocus
                />
            </StyledInputBase>
        </Search>
    );
}
