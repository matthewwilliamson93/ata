import Box from "@mui/material/Box";
import Drawer from "@mui/material/Drawer";
import List from "@mui/material/List";
import Divider from "@mui/material/Divider";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import HomeIcon from "@mui/icons-material/Home";
import FlightTakeoffIcon from "@mui/icons-material/FlightTakeoff";

import React, { Dispatch, SetStateAction } from "react";

/*
 * A React component that is meant show all planned trips.
 *
 * https://mui.com/material-ui/react-drawer/
 */
export default function TravelDrawer({
    trips,
    active,
    setActive,
    setCurrentTrip,
}: {
    trips: string[];
    active: boolean;
    setActive: Dispatch<SetStateAction<boolean>>;
    setCurrentTrip: Dispatch<SetStateAction<string | null>>;
}): JSX.Element {
    return (
        <div>
            <React.Fragment key="travel-drawer">
                <Drawer anchor="left" open={active} onClose={() => setActive(false)}>
                    <Box sx={{ width: 250 }} role="presentation" onClick={() => setActive(false)}>
                        <List>
                            <ListItem key="bucket-list" disablePadding>
                                <ListItemButton onClick={() => setCurrentTrip(null)}>
                                    <ListItemIcon>
                                        <HomeIcon />
                                    </ListItemIcon>
                                    <ListItemText primary="Your Bucket List" />
                                </ListItemButton>
                            </ListItem>
                        </List>
                        <Divider />
                        <List>
                            {trips.map((trip) => (
                                <ListItem key={trip} disablePadding>
                                    <ListItemButton onClick={() => setCurrentTrip(trip)}>
                                        <ListItemIcon>
                                            <FlightTakeoffIcon />
                                        </ListItemIcon>
                                        <ListItemText primary={trip} />
                                    </ListItemButton>
                                </ListItem>
                            ))}
                        </List>
                    </Box>
                </Drawer>
            </React.Fragment>
        </div>
    );
}
