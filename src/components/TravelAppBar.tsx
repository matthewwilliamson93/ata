import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import InputBase from "@mui/material/InputBase";
import MenuIcon from "@mui/icons-material/Menu";

import React, { Dispatch, SetStateAction } from "react";

import TravelInput from "./TravelInput";
import TravelPlannerButton from "./TravelPlannerButton";
import { Route } from "../utils/routes";

export default function TravelAppBar({
    currentTrip,
    isMenuActive,
    onMenuClick,
    route,
    setRoute,
}: {
    currentTrip: string | null;
    isMenuActive: boolean;
    onMenuClick: Dispatch<SetStateAction<boolean>>;
    route: Route;
    setRoute: Dispatch<SetStateAction<Route>>;
}) {
    const isHome = currentTrip === null;
    const header = isHome ? "The Bucket List" : `Lets plan our trip around ${currentTrip}`;

    return (
        <Box sx={{ flexGrow: 1 }}>
            <AppBar>
                <Toolbar>
                    <IconButton
                        size="large"
                        edge="start"
                        color="inherit"
                        aria-label="open drawer"
                        sx={{ mr: 2 }}
                        onClick={() => onMenuClick(!isMenuActive)}
                    >
                        <MenuIcon />
                    </IconButton>
                    <Typography
                        variant="h6"
                        noWrap
                        component="div"
                        sx={{ display: { xs: "none", sm: "block" } }}
                    >
                        {header}
                    </Typography>
                    <TravelInput isHome={isHome} route={route} setRoute={setRoute} />
                    <Box sx={{ flexGrow: 1 }} />
                    <Box sx={{ display: { xs: "none", md: "flex" } }} />
                    {isHome ? null : <TravelPlannerButton route={route} setRoute={setRoute} />}
                </Toolbar>
            </AppBar>
        </Box>
    );
}
