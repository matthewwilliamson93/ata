import IconButton from "@mui/material/IconButton";
import CircularProgress from "@mui/material/CircularProgress";

import ExploreIcon from "@mui/icons-material/Explore";

import React, { Dispatch, SetStateAction, useState, useEffect } from "react";

import { Route, planRoute } from "../utils/routes";

/*
 * A React component that is meant plan a trip.
 *
 * https://mui.com/components/
 */
export default function TravelPlannerButton({
    route,
    setRoute,
}: {
    route: Route;
    setRoute: Dispatch<SetStateAction<Route>>;
}): JSX.Element {
    const [isLoading, setLoading] = useState(false);

    useEffect(() => {
        if (isLoading) {
            const plan = planRoute(route);
            setRoute(plan);
            setLoading(false);
        }
    }, [isLoading]);

    return (
        <div>
            {isLoading ? (
                <CircularProgress />
            ) : (
                <IconButton
                    size="large"
                    aria-label="Plan It!"
                    color="inherit"
                    onClick={() => setLoading(true)}
                >
                    <ExploreIcon />
                </IconButton>
            )}
        </div>
    );
}
